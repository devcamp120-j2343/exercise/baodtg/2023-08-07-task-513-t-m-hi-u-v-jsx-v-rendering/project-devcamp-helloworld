import image from './assets/images/logo-og.png'

const gDevcampReact = {
  title: 'Chào mừng đến với Devcamp React',
  image,
  benefits: ['Blazing Fast', 'Seo Friendly', 'Reusability', 'Easy Testing', 'Dom Virtuality', 'Efficient Debugging'],
  studyingStudents: 50,
  totalStudents: 100
}

const tyLeSinhVienTheoHoc = () => {
    return gDevcampReact.studyingStudents / gDevcampReact.totalStudents * 100
  }
export {gDevcampReact, tyLeSinhVienTheoHoc}