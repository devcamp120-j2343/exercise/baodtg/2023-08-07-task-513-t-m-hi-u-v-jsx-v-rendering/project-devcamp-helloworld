import { gDevcampReact, tyLeSinhVienTheoHoc } from "./info";



function App() {
  return (
    <div className="App">
      <h1> {gDevcampReact.title} </h1>
      <img src={gDevcampReact.image} width={"1000px"}></img>
      <p> Tỷ lệ sinh viên theo học: {tyLeSinhVienTheoHoc()}%</p>
      {tyLeSinhVienTheoHoc() > 15 ? "Sinh viên đăng kí học nhiều" : "Sinh viên đăng kí học ít"}
      <ul>
        {
          gDevcampReact.benefits.map((value, index) => {
            return <li key={index}>{
              value
            }</li>
          })
        }

      </ul>


    </div>
  );
}

export default App;
